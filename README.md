git clone .....

cd node_backend_template

npm i

a bejelentkezeshez:
mongo shell megnyitsas, felhasznalo felvitele kezzel:
az adatbazist es a collectiont nem kell kulon letrehozni, a lenti parancsokkal magatol letrejon
    mongo
    use myitsolver
    db.users.insert({name: 'elek', password: '1234'})
    
configure mailer: 
    https://nodemailer.com/about/
    ./config/mail.js
    
dev:
    nodemon
    
swagger documentation:
    localhost:3000/api-docs