const {Session} = require ('../mongoschemas/sessions')

/**
* Middleware autentikáció. A headerben kapott x-access-token-t hitelesíti. Ha rendben van, meghívja a követlező apit.
* @route post /request
* @group Middleware - Autentikációval kapcsolatos műveletek.
* @returns {Error}  401 - Hibás x access token.
*/
// Check if received session id is valid
module.exports = {
  authSession: (req,res,next) => {
    Session.find({}, function(err, sessions) {
      if (err) {
        res.status(500).send(err)
      }
      else {
        let loggedin = false
        sessions.map(session => {
          loggedin = JSON.parse(session.session).session_id === req.headers['x-access-token']
        })
        if (loggedin) {
          return next()
        } else {
          return res.sendStatus(401)
        }
      }
    })
  }
}
