const {url, connection} = require('./config/mongo.js')
const {swaggerOptions}  = require('./config/swagger.js')
const express           = require('express')
const bodyParser        = require('body-parser')
const cors              = require('cors')
const session           = require('express-session')
const MongoStore        = require('connect-mongo')(session)
const mongoose          = require ('mongoose')
const app               = express()
const expressSwagger    = require('express-swagger-generator')(app)
const port              = 3000

// http://localhost:3000/api-docs
expressSwagger(swaggerOptions)

// MongoStore automatically removes expried sessions
app.use(session({
  key: 'SESSION_ID',
  secret: 'I didnt think I needed a secret, but the voices in my head told me Express needed one',
  saveUninitialized: false, // don't create session until something stored
  resave: false, //don't save session if unmodified
  cookie:{
    secure: false,
    httpOnly: false
  },
  store: new MongoStore({
    url: url,
    touchAfter: 24 * 3600
  })
}))


app.use(cors())
app.use(bodyParser.json())

// Import custom routes
var auth = require ('./routes/auth.js')
var incoming = require ('./routes/incoming.js')
var requests = require ('./routes/requests.js')

// Register custom routes
app.use ('/auth', auth)
app.use ('/incoming', incoming)
app.use ('/request', requests)


app.listen(port, () => console.log(`Example app listening on port ${port}!`))
