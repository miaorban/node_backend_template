const mongoose = require ('mongoose')
const Admin = mongoose.mongo.Admin
const connection = mongoose.createConnection ("mongodb://localhost/myitsolver", {useNewUrlParser: true})
module.exports = {
  connection : connection,
  url        : "mongodb://localhost/myitsolver",
}
