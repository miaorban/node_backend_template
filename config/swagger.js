const options = {
    swaggerDefinition: {
        info: {
            description: 'This is the documentation of the API used behing MyITSolver admin dashboard.',
            title: 'MyItSolver api',
            version: '1.0.0',
        },
        host: 'localhost:3000',
        basePath: '',
        produces: [
            "application/json",
            "application/xml"
        ],
        schemes: ['http', 'https'],
        securityDefinitions: {
            SESSION_ID: {
                type: 'apiKey',
                in: 'header',
                name: 'x-access-token',
                description: "MongoStoreban tárolt session id.",
            }
        }
    },
    basedir: __dirname, //app absolute path
    files: ['../mongoschemas/*.js', '../routes/*.js', '../middleware/*.js'] //Path to the API handle folder
}

module.exports = {
  swaggerOptions : options,
}
