const {connection} = require ('../config/mongo.js')
const mongoose = require ('mongoose')

Schema = mongoose.Schema
/**
 * @typedef ContactRequest
 * @property {string} name.required - Megkereső neve - eg: Kiss Béla
 * @property {string} email.required - Megkereső e-mail címe. - eg: kissbela@gmail.com
 * @property {string} phone.required - Megkereső telefonszáma.
 * @property {string} message - Megkeresés tárgya. - eg: Szeretnék egy weboldalat tegnapra, de nem tudom, hogy mi legyen rajta.
 * @property {string} date.required - Megkeresés ideje. Alapértelmezetten most. - eg: Alapértelmezetten most.
 */
contactSchema = new Schema ({
  name: {type: String, required: true},
  email: {type: String, required: true},
  phone: {type: String, required: true},
  message: {type: String},
  date: {type: Date, required: true, default: Date.now()}
})

const ContactRequest = connection.model ('ContactRequest', contactSchema)

module.exports = {
  ContactRequest
}
