// Registration:
const {connection} = require ('../config/mongo.js')
const mongoose = require ('mongoose')

Schema = mongoose.Schema
/**
 * @typedef User
 * @property {string} name.required - Felhasználónév.
 * @property {string} password.required - Plain text jelszó (csak most, az egyszerűség kedvéért).
 */
userSchema = new Schema ({
  name: {type: String, required: true},
  password: {type: String, required: true}
})

const User = connection.model ('User', userSchema)

module.exports = {
  User
}
