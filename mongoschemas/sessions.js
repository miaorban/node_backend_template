const {connection} = require ('../config/mongo.js')
const mongoose = require ('mongoose')

sessionSchema = new Schema ({
  expires : {type: Date, required: true},
  lastModified : {type: Date, required: true},
  session : {type: Object, required: true}
})

const Session = connection.model ('Session', sessionSchema)

module.exports = {
  Session
}
