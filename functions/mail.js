const {transporter} = require ('../config/mail')

// Send email notification
module.exports = function ({ name, phone, email, message}) {
  var mailOptions = {
    from: 'info@myitsolver.com',
    subject: 'Új megkeresés',
    text: `Szia, új megkeresésetek érkezett. Feladó: ${name} (${phone}, ${email}), üzenet: ${message}`
  }

  transporter.sendMail(mailOptions, function(error, info){
    console.log('error: ', error)
    console.log('info: ', info)
  })
};
