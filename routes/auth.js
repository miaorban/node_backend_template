const {User}     = require ('../mongoschemas/users')
const {Session}  = require ('../mongoschemas/sessions')
const express    = require ('express')
const router     = express.Router()
const session    = require('express-session')
const MongoStore = require('connect-mongo')(session)

/**
* Felhasználó bejelentkeztetése. Az express-session létrehoz a MongoStoreban egy sessiont.
* @route post /auth/login
* @group Autentikáció - Felhasználókezeléssel kapcsolatos műveletek.
* @param {string} id.param.required - Megkeresés mongo idja.
* @returns {Object} 200 - {success: 1, msg: 'Sikeres bejelentkezés.', session: express session által generált id} <br> {success: 0, msg: 'Hibás felhasználónév vagy jelszó.'} <br> {success: 1, msg: 'Sikeres bejelentkezés.', session: express session által generált id}
* @returns {Error}  500 - váratlan hiba
*/
router.post('/login', function(req, res){
  User.findOne({name: req.body.username}, function(err, user) {
    if (err) {
      res.status(500).send({success: 0, msg: err})
    }
    else {
      if (user === null) {
        res.status(200).send({success: 0, msg: 'Hibás felhasználónév vagy jelszó.'})
      } else {
        if (user.password === req.body.password) {
          // initialise session -> mongostore saves it
          req.session.session_id = req.sessionID
          res.status(200).send({success: 1, msg: 'Sikeres bejelentkezés.', session: req.sessionID})
        } else {
          res.status(200).send({success: 0, msg: 'Hibás felhasználónév vagy jelszó.'})
        }
      }
    }
  })
})

/**
* Felhasználó kijelentkeztetése. Session törlése.
* @route get /auth/logout
* @group Autentikáció - Felhasználókezeléssel kapcsolatos műveletek.
* @returns {Object} 200 - {success: 0, msg: 'Error while deleting sessionid'} <br> {success: 1, msg: 'Session deleted'}
* @returns {Error}  500 - váratlan hiba
*/
router.get('/logout', function(req, res){
  // Session find not returning document ids
  Session.find({}, function(err, sessions) {
    if (err) {
      res.status(500).send(err)
    }
    else {
      let usersession
      sessions.map(session => {
        if (JSON.parse(session.session).session_id === req.headers['x-access-token']) {
          usersession = session.session
        }
      })
      Session.deleteOne({session: usersession}, function (error, result) {
        let msg, success
        if (error || result.n == 0) {
          msg = 'Error while deleting sessionid'
          success = 0
        } else {
          msg = 'Session deleted'
          success = 1
        }
        res.status(200).send({success: success, msg: msg})
      })
    }
  })
})


module.exports = router
