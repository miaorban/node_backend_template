const {ContactRequest} = require ('../mongoschemas/requests')
const {authSession} = require ('../middleware/authSession')
const mailer = require('../functions/mail')
var ObjectId = require('mongodb').ObjectID

const express = require ('express')
const router = express.Router()

// Authenticate user
router.use(authSession)

/**
* Eddig megkeresések listázása.
* @route get /request/collect/basic
* @group Megkeresések - Beérkezett megkeresések kezelése
* @returns {Array<ContactRequest.model>} 200 - Eddigi megkeresések, nem jelenítjük meg az üzenetet.
* @returns {Error}  500 - váratlan hiba
* @security SESSION_ID
*/
router.get('/collect/basic', function(req, res){
  ContactRequest.find({}, 'name phone email', function(err, requests) {
    if (err) {
      res.status(500).send(err)
    }
    else {
      res.status(200).send(requests)
    }
  })
})

/**
* Egy megkeresés adatainak listázása.
* @route get /request/find/:id
* @group Megkeresések - Beérkezett megkeresések kezelése
* @param {string} id.param.required - Megkeresés mongo idja.
* @returns {ContactRequest.model} 200 - Keresett megkeresés minden adatával.
* @returns {Error}  500 - váratlan hiba
* @security SESSION_ID
*/
router.get('/find/:id', function(req, res){
  ContactRequest.findById(req.params.id, function (err, request) {
    if (err) {
      res.status(500).send(err)
    }
    else {
      res.status(200).send(request)
    }
  })
})


module.exports = router
