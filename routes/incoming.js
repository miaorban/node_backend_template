const mailer            = require('../functions/mail')
const {ContactRequest}  = require ('../mongoschemas/requests')
const express           = require ('express')
const router            = express.Router()

/**
* Új megkeresés mentése.
* @route POST /incoming/new
* @group Megkeresések - Beérkezett megkeresések kezelése
* @param {ContactRequest.model} request.body.required - Menteni kívánt megkeresés.
* @returns {string} 200 - 'ok' - sikeres mentés
* @returns {Error}  500 - váratlan hiba
* @security SESSION_ID
*/
router.post('/new', function(req, res) {
  let request = new ContactRequest(req.body)
  request.save (function (err, success) {
    if (err) {
      if (err.name === 'ValidationError') res.status(200).send({success: 0, msg: 'A csillaggal jelölt mezők kitöltése közelező!'})
      else res.status(500).send({success: 0, msg: 'Hiba történt az üzeneted továbbítása közben.'})
    }
    else {
      mailer(req.body)
      res.status(201).send({success: 1, msg: 'Köszönjük a megkeresést, hamarosan felvesszük Önnek a kapcsolatot!'})
    }
  })
})

module.exports = router
